import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Products } from 'src/database/entities/product.entity';
import { MulterModule } from '@nestjs/platform-express';
import { InventoryModule } from '../inventory/inventory.module';
import { Cve } from 'src/database/entities/cve.entity';
import { Cpe } from 'src/database/entities/cpe.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Products, Cve, Cpe]),
    MulterModule.register({
      dest: 'upload',
    }),
    InventoryModule,
  ],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}
