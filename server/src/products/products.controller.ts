import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
  UseGuards,
  Delete,
  Put,
  ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ProductDTO } from 'src/resources/models/product/product.dto';
import { SearchDTO } from 'src/resources/models/search.dto';
import { ProductsService } from './products.service';
import { AuthGuard } from '@nestjs/passport';
import { ResponseMessageDTO } from 'src/resources/models/response.message.dto';
import { AddProductDTO } from 'src/resources/models/product/add-product.dto';

@Controller('products')
@UseGuards(AuthGuard('jwt'))
export class ProductsController {
  constructor(private readonly productService: ProductsService) {}

  @Get()
  public async getProducts(
    @Query('vulnerable') cve: boolean,
    @Query('start') start: number,
    @Query('limit') limit: number,
  ): Promise<ProductDTO[]> {
    return await this.productService.getProducts(start, limit, cve);
  }

  @Get(':id')
  public async getProductById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ProductDTO> {
    return await this.productService.getProductById(id);
  }

  @Delete(':id')
  public async deleteProductById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ResponseMessageDTO> {
    return await this.productService.deleteProduct(id);
  }

  @Post()
  public async addNewProduct(@Body(ValidationPipe) body: AddProductDTO): Promise<ProductDTO> {
    return await this.productService.addNewProduct(body);
  }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  public async uploadFile(@UploadedFile() file): Promise<any[]> {
    return await this.productService.uploadProducts(file);
  }

  @Post('/search')
  public async searchProduct(@Query() querySearch: SearchDTO) {
    if (querySearch.query) {
      return await this.productService.searchProduct(querySearch);
    }
  }

  @Put(':productId/cve/:cveId')
  public async addCveToProductManually(
    @Param('productId', ParseIntPipe) productId: number,
    @Param('cveId', ParseIntPipe) cveId: number,
  ): Promise<ResponseMessageDTO> {
    return await this.productService.addCveToProductManually(productId, cveId);
  }

  @Put(':productId/cpe/:cpeId')
  public async addCpeToProductManually(
    @Param('productId', ParseIntPipe) productId: number,
    @Param('cpeId', ParseIntPipe) cpeId: number,
  ): Promise<ResponseMessageDTO> {
    return await this.productService.addCpeToProductManually(productId, cpeId);
  }

  @Delete(':productId/cve/:cveId')
  public async deleteCveRelation(
    @Param('productId', ParseIntPipe) productId: number,
    @Param('cveId', ParseIntPipe) cveId: number,
  ): Promise<ResponseMessageDTO> {
    return await this.productService.deleteCveRelation(productId, cveId);
  }

  @Delete(':productId/cpe/:cpeId')
  public async deleteCpeRelation(
    @Param('productId', ParseIntPipe) productId: number,
    @Param('cpeId', ParseIntPipe) cpeId: number,
  ): Promise<ResponseMessageDTO> {
    return await this.productService.deleteCpeRelation(productId, cpeId);
  }
}
