import {
  Injectable,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import { Products } from 'src/database/entities/product.entity';
import { ProductDTO } from 'src/resources/models/product/product.dto';
import { SearchDTO } from 'src/resources/models/search.dto';
import { Like, Repository, getConnection } from 'typeorm';
import { Cve } from '../database/entities/cve.entity';
import { CpeService } from '../inventory/cpe.service';
import { CveService } from '../inventory/cve.service';
import { Cpe } from 'src/database/entities/cpe.entity';
import { ResponseMessageDTO } from 'src/resources/models/response.message.dto';
import { AddProductDTO } from 'src/resources/models/product/add-product.dto';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Products)
    private readonly productRepository: Repository<Products>,
    @InjectRepository(Cve) private readonly cveRepository: Repository<Cve>,
    @InjectRepository(Cpe) private readonly cpeRepository: Repository<Cpe>,
    private readonly cpeService: CpeService,
    private readonly cveService: CveService,
  ) {}

  public async getProducts(start = 0, limit = 15, cve): Promise<Products[]> {
    if (limit > 25) {
      limit = 25;
    }

    if (cve) {
      return await this.productRepository.find({
        skip: start,
        take: limit,
        where: { vulnerable: cve },
      });
    } else {
      return await this.productRepository.find({
        skip: start,
        take: limit,
      });
    }
  }

  public async getProductById(id: number): Promise<Products> {
    const foundProduct = await this.productRepository.findOne({
      relations: ['cpe_list', 'cve_list'],
      where: { id },
    });

    return await foundProduct;
  }

  public async addNewProduct(body: AddProductDTO): Promise<ProductDTO> {
    const { name, vendor, version } = body;
    const cpePartial = { product: name, vendor, version };
    const potentialFoundProduct = await this.productRepository.findOne(body);

    if (potentialFoundProduct === undefined) {
      const inventory = new Products();
      inventory.name = name;
      inventory.vendor = vendor;
      inventory.version = version;

      const foundCpes = await this.cpeService.matchCpeToProduct(cpePartial);
      const foundCves: Cve[] = [];
      const vulnerability = [];

      for (const cpe of foundCpes.cpeList) {
        const cpeCves = await this.cveService.matchCveToCpe(cpe);
        cpeCves.forEach(cve => {
          const alreadyIncluded = foundCves.filter(
            includedCve => includedCve.cve === cve.cve,
          );
          if (alreadyIncluded.length === 0) {
            foundCves.push(cve);
          }
          vulnerability.push(
            ...cve.cpe_list.filter(
              cpeListItem => cpeListItem.vulnerable === true,
            ),
          );
        });
      }

      inventory['cpe_list'] = foundCpes.cpeList;
      inventory['cve_list'] = foundCves;
      inventory.vulnerable = vulnerability.length !== 0;
      inventory.match = foundCpes.exact;

      const savedProduct = await this.productRepository.save(inventory);
      return savedProduct;
    }
  }

  public async searchProduct(querySearch: SearchDTO) {
    const { query } = querySearch;
    const where: any = {};
    const whereArray = [];
    let outsideQuery: any;
    const results = [];

    if (query.includes('vendor:')) {
      const splitVendor = this.splitQuery(query, 'vendor');
      whereArray.push(`vendor:${splitVendor}`);
      where.vendor = Like(`%${splitVendor}%`);
      results.push(...(await this.searchByVendorOrName(where)));
    }

    if (query.includes('name:')) {
      const splitName = this.splitQuery(query, 'name');
      whereArray.push(`name:${splitName}`);
      where.name = Like(`%${splitName}%`);
      if (results.length === 0) {
        results.push(...(await this.searchByVendorOrName(where)));
      } else {
        const query = await this.searchByVendorOrName(where);
        results.filter(result => {
          query.includes(result);
        });
      }
    }

    if (query.includes('cpe:')) {
      const splitCpe = this.splitQuery(query, 'cpe');
      const convertToNumber = Number(splitCpe);
      whereArray.push(`cpe:${splitCpe}`);

      if (convertToNumber) {
        if (results.length === 0) {
          results.push(...(await this.searchByCpeId(convertToNumber)));
        } else {
          const query = await this.searchByCpeId(convertToNumber);
          results.filter(result => {
            query.includes(result);
          });
        }
      }
    }

    if (query.includes('cve:')) {
      const splitCve = this.splitQuery(query, 'cve');
      const convertToNumber = Number(splitCve);
      whereArray.push(`cve:${splitCve}`);

      if (convertToNumber) {
        if (results.length === 0) {
          results.push(...(await this.searchByCveId(convertToNumber)));
        } else {
          const query = await this.searchByCveId(convertToNumber);
          results.filter(result => {
            query.includes(result);
          });
        }
      }
    }

    outsideQuery = query;
    whereArray.forEach(condition => {
      outsideQuery = query.replace(condition, '');
    });

    if (results.length === 0) {
      results.push(
        ...(await this.searchByVendorOrName(
          `vendor ILIKE '%${outsideQuery}%' OR name ILIKE '%${outsideQuery}%'`,
        )),
      );
    } else {
      const query = await this.searchByVendorOrName(
        `vendor ILIKE '%${outsideQuery}%' OR name ILIKE '%${outsideQuery}%'`,
      );
      results.filter(result => {
        query.includes(result);
      });
    }

    return results;
  }

  public async uploadProducts(file): Promise<any[]> {
    const filePath = `${file.destination}/${file.filename}`;
    const matchedCpes = [];
    await new Promise((resolve, reject) => {
      fs.readFile(filePath, async (err, data) => {
        if (err) {
          reject(err);
          throw new InternalServerErrorException(err);
        }

        const uploadedProducts = JSON.parse(data.toString());

        for (const product of uploadedProducts) {
          const productToAdd = this.productRepository.create({
            name: product.product,
            vendor: product.vendor,
            version: product.version,
          });

          const existingProduct = await this.productRepository.findOne(
            productToAdd,
          );

          if (!existingProduct) {
            const matchedProducts = await this.addNewProduct(productToAdd);
            matchedCpes.push(matchedProducts);
          } else {
          }
        }
        resolve(data);
      });
    });
    fs.unlinkSync(filePath);
    return matchedCpes;
  }

  public async deleteProduct(productId: number) {
    const foundProduct = await this.checkProductExist(productId);
    await this.productRepository.delete(productId);
    return { msg: `${foundProduct.name} deleted!` };
  }

  public async addCveToProductManually(
    productId: number,
    cveId: number,
  ): Promise<ResponseMessageDTO> {
    const product = await this.checkProductExist(productId);

    try {
      await getConnection()
        .createQueryBuilder()
        .relation(Products, 'cve_list')
        .of(productId)
        .add(cveId);
    } catch (err) {
      throw new BadRequestException('They are matched!');
    }

    return {
      msg: 'Successful added!',
    };
  }

  public async addCpeToProductManually(
    productId: number,
    cpeId: number,
  ): Promise<ResponseMessageDTO> {
    const product = await this.checkProductExist(productId);
    const vulnerabilities = [];

    try {
      await getConnection()
        .createQueryBuilder()
        .relation(Products, 'cpe_list')
        .of(productId)
        .add(cpeId);
    } catch (err) {
      throw new BadRequestException('They are matched!');
    }

    return {
      msg: 'Successful added!',
    };
  }

  public async deleteCveRelation(
    productId: number,
    cveId: number,
  ): Promise<ResponseMessageDTO> {
    const cve = await this.productRepository.findOne({
      where: {
        id: productId,
      },
      relations: ['cve_list'],
    });

    const matchIds = cve.cve_list.some(el => {
      return el.id === cveId;
    });

    if (!matchIds) {
      throw new BadRequestException('There is no such a cve');
    }

    await getConnection()
      .createQueryBuilder()
      .relation(Products, 'cve_list')
      .of(productId)
      .remove(cveId);

    return {
      msg: 'Successful delete!',
    };
  }

  public async deleteCpeRelation(
    productId: number,
    cpeId: number,
  ): Promise<ResponseMessageDTO> {
    const cpe = await this.productRepository.findOne({
      where: {
        id: productId,
      },
      relations: ['cpe_list'],
    });

    const matchIds = cpe.cpe_list.some(el => {
      return el.id === cpeId;
    });

    if (!matchIds) {
      throw new BadRequestException('There is no such a cpe');
    }

    await getConnection()
      .createQueryBuilder()
      .relation(Products, 'cpe_list')
      .of(productId)
      .remove(cpeId);

    return {
      msg: 'Successful delete!',
    };
  }

  private splitQuery(search: string, query: string): string {
    const splitSearch = search.split(' ');
    const valueIndex = splitSearch.findIndex(el => el.includes(`${query}:`));

    return splitSearch[valueIndex].split(`${query}:`)[1];
  }

  private async searchByVendorOrName(parameters: any) {
    const foundProduct: Products[] = await this.productRepository.find({
      where: parameters,
      select: ['id', 'name', 'vendor', 'version', 'createdOn'],
    });

    return foundProduct;
  }

  private async searchByCveId(id: number) {
    const foundCves: Cve[] = await this.cveRepository.find({
      where: {
        id,
      },
    });

    return foundCves;
  }

  private async searchByCpeId(id: number) {
    const foundCpes: Cpe[] = await this.cpeRepository.find({
      where: {
        id,
      },
    });

    return foundCpes;
  }

  private async checkProductExist(productId: number): Promise<Products> {
    const product = await this.productRepository.findOne({
      where: {
        id: productId,
      },
    });

    if (!product) {
      throw new BadRequestException('There is no such a product');
    }

    return product;
  }
}
