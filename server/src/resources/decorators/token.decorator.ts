import { createParamDecorator } from "@nestjs/common";
import { Request } from 'express';

export const Token = createParamDecorator((_, req: Request) => req.headers.authorization);