import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { User } from 'src/database/entities/user.entity';

@Injectable()
export class AdminGuard implements CanActivate {
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const requestUser = request.user;
    
    const user = await getConnection().manager.findOne(User, { username: requestUser.username });
    user.roles = await getConnection()
      .createQueryBuilder()
      .relation(User, 'roles')
      .of(user)
      .loadMany();

    const isAdmin = user.roles.some((el) => {
      if (el.name === 'Admin') {
        return el;
      }
    });

    if (!isAdmin) {
      throw new ForbiddenException('You are not allowed to make this operation.');
    }

    return true;
  }
}
