import { ProductDTO } from '../product/product.dto';
import { CpeDTO } from './cpe.dto';

export class CpeMatchesDTO {
  product: ProductDTO;
  matches: CpeDTO[];
}
