export class CpeDTO {
  id: number;

  title: string;

  cpe23Uri: string;

  product: string;

  vendor: string;

  version: string;
}
