import { IsNotEmpty, IsString, MinLength } from "class-validator";

export class AddProductDTO {
    @MinLength(2, {
        message: 'Product name should be at least 2 characters'
    })
    name: string;

    @MinLength(2, {
        message: 'Product name should be at least 2 characters'
    })
    vendor: string;

    @IsNotEmpty()
    @IsString()
    version: string;
}