export class ProductDTO {
  name: string;
  vendor: string;
  version: string;
  vulnerable: boolean;
  match: boolean;
}
