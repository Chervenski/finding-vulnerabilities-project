import { IsEmail, Matches } from 'class-validator';

export class UserLoginDTO {
  @IsEmail()
  public email?: string;

  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/, {
    message:
      'The password must be minimum six characters long and contain at least one letter and one number!',
  })
  public password: string;
}
