import { Expose } from 'class-transformer';
import { UserRole } from '../../enums/user-roles.enum';

export class UserReturnDTO {
  @Expose()
  public username: string;
  
  @Expose()
  public name: string;

  @Expose()
  public id: number;

  @Expose()
  public email: string;

  @Expose()
  public roles: UserRole[];

  @Expose()
  public joinedOn: string;
}
