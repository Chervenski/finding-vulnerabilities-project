import { IsEmail, Length, Matches } from 'class-validator';

export class UserRegisterDTO {
  @Length(2, 35)
  name: string;

  @Length(4, 60)
  @IsEmail()
  email: string;

  @Length(2, 16)
  username: string;

  @Length(6, 32)
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/, {
    message:
      'The password must be minimum six characters long and contain at least one letter and one number!',
  })
  password: string;
}
