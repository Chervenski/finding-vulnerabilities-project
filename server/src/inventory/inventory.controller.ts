import { Body, Controller, Post, Put, Get } from '@nestjs/common';
import { LinkDTO } from 'src/resources/models/link.dto';
import { CveService } from './cve.service';
import { ResponseMessageDTO } from 'src/resources/models/response.message.dto';

@Controller('inventory')
export class InventoryController {
  constructor(private readonly cveService: CveService) {}

  @Post()
  public async getCveList(@Body() link: LinkDTO): Promise<ResponseMessageDTO> {
    return await this.cveService.getCveList(link.link);
  }

  @Get('/refresh')
  public async updateCveList() {
    return await this.cveService.updateCveDB();
  }
}
