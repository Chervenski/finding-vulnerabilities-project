import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cpe } from 'src/database/entities/cpe.entity';
import { Cve } from 'src/database/entities/cve.entity';
import { Products } from 'src/database/entities/product.entity';
import { CveService } from './cve.service';
import { InventoryController } from './inventory.controller';
import { CpeService } from './cpe.service';
import { Link } from 'src/database/entities/link.entity';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([Products, Cve, Cpe, Link])],
  controllers: [InventoryController],
  providers: [CveService, CpeService],
  exports: [CveService, CpeService],
})
export class InventoryModule {}
