import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import { Repository } from 'typeorm';
import { Cve } from '../database/entities/cve.entity';
import { CpeDTO } from '../resources/models/inventory/cpe.dto';
import { LinkDTO } from 'src/resources/models/link.dto';
import { Link } from 'src/database/entities/link.entity';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ResponseMessageDTO } from 'src/resources/models/response.message.dto';

@Injectable()
export class CveService {
  constructor(
    private readonly httpService: HttpService,
    @InjectRepository(Cve) private readonly cveRepository: Repository<Cve>,
    @InjectRepository(Link) private readonly linkRepository: Repository<Link>,
  ) {}

  public async matchCveToCpe(cpe: CpeDTO): Promise<Cve[]> {
    const { product, vendor, version } = cpe;

    const found = await this.cveRepository.find({
      where: `"cpe_list"::TEXT ILIKE '%${product}%' AND "cpe_list"::TEXT ILIKE '%${vendor}%'`,
    });

    const cveMatches = found.filter(cve => {
      const versionMatch = cve['cpe_list'].map(cpeListItem => {
        if (
          cpeListItem.cpe23Uri.split(':')[5].includes(version) ||
          cpeListItem.cpe23Uri.split(':')[5].includes('*') ||
          cpeListItem.cpe23Uri.split(':')[5].includes('-')
        ) {
          return true;
        } else {
          return false;
        }
      });
      return versionMatch.includes(true);
    });

    return cveMatches;
  }

  public async getCveList(downloadUrl: string): Promise<ResponseMessageDTO> {
    await this.saveLinkToDb(downloadUrl);

    const url = downloadUrl;
    const zipPath = './upload/tempfile.zip';
    const writer = fs.createWriteStream(zipPath);
    const newlyFetchedCves: Cve[] = [];
    const response = await this.httpService.axiosRef({
      url,
      method: 'GET',
      responseType: 'stream',
    });

    response.data.pipe(writer);

    await new Promise(resolve => {
      writer.on('finish', async () => {
        newlyFetchedCves.push(...(await this.unzipCveFile(zipPath)));
        fs.unlinkSync(zipPath);
        resolve();
      });
      writer.on('error', reject => {
        console.log(reject);
      });
    });
    return {
      msg: `${newlyFetchedCves.length} new CVEs successfully fetched!`,
    };
  }

  private async unzipCveFile(file): Promise<Cve[]> {
    const unzip = require('node-stream-zip');
    const addedCves: Cve[] = [];

    const zip = new unzip({
      file,
      storeEntries: true,
    });
    await new Promise(resolve => {
      zip.on('ready', async () => {
        for await (const entry of Object.values(zip.entries())) {
          const fileName = (entry as any).name;
          if (fileName.includes('nvdcve') && fileName.includes('.json')) {
            const cveList = JSON.parse(zip.entryDataSync(fileName).toString());
            const cveItems = cveList['CVE_Items'];
            for await (const element of cveItems) {
              const elementToAdd = await this.saveCveToDb(element);
              if (elementToAdd !== undefined) {
                addedCves.push(elementToAdd);
              }
            }
          }
        }
        zip.close();
        return resolve();
      });
    });

    return addedCves;
  }

  private async saveCveToDb(cveToAdd): Promise<Cve> {
    if (cveToAdd.configurations?.nodes?.length !== 0) {
      const cve: string = cveToAdd.cve['CVE_data_meta']['ID'];
      const assigner: string = cveToAdd.cve['CVE_data_meta']['ASSIGNER'];
      const description: string =
        cveToAdd.cve.description['description_data'][0].value;
      const publishedDate: string = cveToAdd.publishedDate;
      const lastModifiedDate: string = cveToAdd.lastModifiedDate;
      const baseScore: string =
        cveToAdd?.impact?.baseMetricV3?.cvssV3.baseScore;
      const baseSeverity: string =
        cveToAdd?.impact?.baseMetricV3?.cvssV3.baseSeverity;

      const vulnerabilityList = await this.findCpesFromCve(
        cveToAdd.configurations.nodes,
      );

      const newCve: Cve = this.cveRepository.create({
        cve,
        description,
        assigner,
        publishedDate,
        lastModifiedDate,
        addedToLocalDbDate: new Date().toString(),
        baseScore,
        baseSeverity,
        cpe_list: vulnerabilityList,
      });

      const potentialExistingCve = await this.cveRepository.findOne({
        where: { cve },
      });

      if (!potentialExistingCve) {
        const savedCve = await this.cveRepository.save(newCve);
        return savedCve;
      } else {
      }
    }
  }

  private async findCpesFromCve(cveNodes: any[]) {
    const cpeArray = [];
    let allowedLevels = 1;
    const recursiveFn = (nodes: any[]) => {
      nodes.forEach(node => {
        if (node.operator === 'AND') {
          if (allowedLevels !== 0 && node.children) {
            recursiveFn(node.children);
            allowedLevels--;
          }
        }
        if (node.operator === 'OR') {
          node['cpe_match'].forEach(match => {
            if (match.cpe23Uri) {
              cpeArray.push({
                vulnerable: match.vulnerable,
                cpe23Uri: match.cpe23Uri,
              });
            }
          });
        }
      });
    };
    recursiveFn(cveNodes);
    return cpeArray;
  }

  private async saveLinkToDb(link: string) {
    const checkLink = await this.linkRepository.findOne({
      where: {
        name: link,
      },
    });

    if (!checkLink) {
      const newLink = new Link();
      newLink.name = link;

      await this.linkRepository.save(newLink);
    }
  }

  @Cron(CronExpression.EVERY_WEEK)
  public async updateCveDB() {
    const links = await this.linkRepository.find();

    if (links.length > 0) {
      for (const link of links) {
        await this.getCveList(link.name);
      }
      return { msg: 'CVEs refreshed!' };
    }
    return { msg: 'No existing CVE links saved!' };
  }
}
