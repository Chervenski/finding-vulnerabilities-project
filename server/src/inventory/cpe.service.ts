import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cpe } from 'src/database/entities/cpe.entity';
import { Repository, Like, Raw } from 'typeorm';
import { CpeDTO } from '../resources/models/inventory/cpe.dto';

@Injectable()
export class CpeService {
  constructor(
    @InjectRepository(Cpe) private readonly cpeRepository: Repository<Cpe>,
  ) {}

  public async matchCpeToProduct(params: Partial<CpeDTO>) {
    const vendor = params.vendor.toLowerCase().split(/(?:,| )+/);
    const product = params.product.toLowerCase().split(/(?:,| )+/);
    const version = params.version;

    const vendorQuery = [];
    const productQuery = [];
    const titleQuery = [];

    const foundProducts: CpeDTO[] = [];
    const returnedCpes: CpeDTO[] = [];

    vendor.forEach(string => {
      vendorQuery.push(`vendor ILIKE '%${string}%'`);
    });
    product.forEach(string => {
      productQuery.push(`product ILIKE '%${string}%'`);
      titleQuery.push(`title ILIKE '%${string}%'`);
    });

    const searchQuery = [
      vendorQuery.join(' AND '),
      'AND',
      productQuery.join(' AND '),
      'OR',
      titleQuery.join(' AND '),
    ].join(' ');

    foundProducts.push(
      ...(await this.cpeRepository.find({
        where: searchQuery,
      })),
    );

    if (foundProducts.length === 0 && titleQuery.length > 2) {
      const vagueSearch = [
        vendorQuery[0],
        'AND',
        titleQuery.slice(0, 2).join(' AND '),
      ].join(' ');
      foundProducts.push(
        ...(await this.cpeRepository.find({
          where: vagueSearch,
        })),
      );
    }

    const versionString = version.split('');
    let exactMatch = true;
    versionString.forEach(character => {
      if (returnedCpes.length === 0) {
        foundProducts.forEach(cpe => {
          if (cpe.version === versionString.join('')) {
            returnedCpes.push(cpe);
          }
        });
      }

      if (returnedCpes.length === 0) {
        exactMatch = false;
        versionString.pop();
      } else {
        return;
      }
    });

    return { cpeList: returnedCpes, exact: exactMatch };
  }
}
