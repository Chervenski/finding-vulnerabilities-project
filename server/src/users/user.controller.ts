import { Param, Post, UseInterceptors, UploadedFile, ParseIntPipe, Controller, UseGuards, Get } from "@nestjs/common";
import { UsersService } from "./users.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { User } from "src/database/entities/user.entity";
import { GetUser } from '../resources/decorators/user.decorator';
import { AuthGuard } from "@nestjs/passport";

@UseGuards(AuthGuard('jwt'))
@Controller('user')
export class UserController {
    constructor(
      private readonly userService: UsersService,
    ) {}

    @Get('/:id')
    public async getUserInfo(
      @Param('id') id: number
    ): Promise<User> {
      return await this.userService.getUserInfo(id);
    }

    @Post('/:id/avatar')
    @UseInterceptors(FileInterceptor('file'))
    public async uploadUserAvatar(
      @Param('id', ParseIntPipe) id: number,
      @UploadedFile() file: any,
      @GetUser() user: User,
    ): Promise<any> {
      return await this.userService.uploadPhoto(id, file, user);
    }
}