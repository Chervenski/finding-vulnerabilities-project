import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { UserReturnDTO } from '../resources/models/user/user.return.dto';
import { plainToClass } from 'class-transformer';
import { User } from '../database/entities/user.entity';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  public async getUserInfo(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);

    return user;
  }
  
  public async getUserByUsername(username: string): Promise<UserReturnDTO> {
    const user: User = await this.userRepository.findOne({
      where: { username, isDeleted: false },
    });

    if (user === undefined) {
      throw new NotFoundException(`User named '${username}' does not exist!`);
    }

    return plainToClass(UserReturnDTO, user, { excludeExtraneousValues: true });
  }

  public async uploadPhoto(id: number, file: any, user: User): Promise<any> {
    user.avatar = file.buffer;
    const savedUser: User = await this.userRepository.save(user);
  
    return savedUser.avatar;
  }
}
