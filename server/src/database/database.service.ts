import {
  HttpService,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import * as fs from 'fs';
import * as unzipper from 'unzipper';

@Injectable()
export class DatabaseService {
  constructor(private readonly httpService: HttpService) {}
  public async getCveList(downloadUrl: string) {
    const url = downloadUrl;
    const zipPath = './temp/tempfile.zip';
    let filePath: string;
    const writer = fs.createWriteStream(zipPath);

    const response = await this.httpService.axiosRef({
      url,
      method: 'GET',
      responseType: 'stream',
    });

    response.data.pipe(writer);

    await new Promise((resolve, reject) => {
      writer.on('finish', resolve);
      writer.on('error', reject);
    });

    fs.createReadStream(zipPath)
      .pipe(unzipper.Parse())
      .on('entry', async entry => {
        if (entry.path) {
          filePath = `temp/${entry.path}`;
          entry.pipe(fs.createWriteStream(filePath)).on('finish', () => {
            fs.readFile(filePath, (err, data) => {
              if (err) {
                throw new InternalServerErrorException(err);
              }
              const result = JSON.parse(data.toString());
              result.CVE_Items.forEach(cve => {
                this.addCveToDatabase(cve);
              });
              fs.unlinkSync(filePath);
            });
          });
        } else {
          entry.autodrain();
        }
      });
    fs.unlinkSync(zipPath);
  }

  public async addCveToDatabase(cve) {
    if (cve.configurations.nodes.length !== 0) {
      console.log(cve);
      console.log(cve.configurations.nodes);
      // console.log(cve.configurations.nodes[0]['cpe_match']);
    }
  }
}
