import { Controller, Get } from '@nestjs/common';
import { DatabaseService } from './database.service';

@Controller('database')
export class DatabaseController {
  constructor(private readonly databaseService: DatabaseService) {}
  @Get()
  public async getCveList() {
    return await this.databaseService.getCveList(
      'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.zip',
    );
  }
}
