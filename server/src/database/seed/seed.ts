import { Repository, In, createConnection } from 'typeorm';
import { UserRole } from '../../resources/enums/user-roles.enum';
import { Role } from '../entities/role.entity';
import { User } from '../entities/user.entity';
import { xmlParser } from './parse-xml';
import * as bcrypt from 'bcrypt';
import { Cpe } from '../entities/cpe.entity';

const seedRoles = async (connection: any) => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Role[] = Object.keys(UserRole).map(
    (roleName: string) => rolesRepo.create({ name: roleName }),
  );

  await rolesRepo.save(rolesSeeding);
  console.log('Seeded roles successfully!');
};

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const admin = await userRepo.findOne({
    where: {
      username: 'Admincho',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const roleNames: string[] = Object.keys(UserRole);

  const allUserRoles: Role[] = await rolesRepo.find({
    where: {
      name: In(roleNames),
    },
  });

  if (allUserRoles.length === 0) {
    console.log('The DB does not have any roles!');
    return;
  }

  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash('realMadrid', salt);

  const newAdmin: User = new User();
  newAdmin.name = 'DoItWise';
  newAdmin.username = 'DoItWiser';
  newAdmin.email = 'doitwise@gmail.com';
  newAdmin.password = hashedPassword;
  newAdmin.roles = allUserRoles;

  const adminUser = userRepo.create(newAdmin);
  await userRepo.save(adminUser);

  console.log('Seeded admin successfully!');
};

const seedData = async (connection: any) => {
  const cpeRepo: Repository<Cpe> = connection.manager.getRepository(Cpe);
  let outCpes: Partial<Cpe>[];

  try {
    [outCpes] = (await xmlParser(
      `official-cpe-dictionary_v2.3.xml`,
    )) as any;
  } catch (error) {
    console.log(error);
  }

  await cpeRepo.save(outCpes, { chunk: 1000 });

  console.log('Seeded xml successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedData(connection);
  await seedRoles(connection);
  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);