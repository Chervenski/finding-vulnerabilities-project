import { Stream } from 'stream';
import * as fs from 'fs';
import * as readline from 'readline';

export const xmlParser = async (inputFile: string) => {
  const cpes: Set<any> = new Set();

  const outstream = new Stream();
  const instream = fs.createReadStream(inputFile);
  const rl = readline.createInterface(instream, outstream as any);
  const outCpes = [];
  let title = '';
  let cpeUri = '';
  let vendor = '';
  let product = '';
  let version = '';

  rl.on(`line`, line => {
    if (line.includes('<title ')) {
      title = line.substring(28, line.length - 8);
    }

    if (line.includes('<cpe-23:cpe23-item')) {
      cpeUri = line.substring(29, line.length - 3);
      const cpeArr = cpeUri.split(':');

      if (cpeArr[2] === 'a') {
        vendor = cpeArr[3];
        product = cpeArr[4];
        version = cpeArr[5];
      }
    }

    if (line.includes(`</cpe-item>`)) {
      if (cpeUri.split(':')[2] === 'a') {
        cpes.add({
          title: title,
          cpe23Uri: cpeUri,
          vendor: vendor,
          product: product,
          version: version,
        });

        title = '';
        cpeUri = '';
        vendor = '';
        product = '';
        version = '';
      }
    }
  });

  return new Promise((resolve, reject) => {
    instream.on('close', () => {
      cpes.forEach(item => {
        outCpes.push(item);
      });
      resolve([outCpes]);
    });

    instream.on('error', reject);
  });
};
