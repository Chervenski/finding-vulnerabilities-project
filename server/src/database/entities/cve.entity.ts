import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  OneToMany,
} from 'typeorm';

@Entity('cves')
export class Cve {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'text', unique: true })
  cve: string;

  @Column('text')
  description: string;

  @Column('text')
  assigner: string;

  @Column('text')
  baseScore: string;
  
  @Column('text')
  baseSeverity: string;

  @Column('timestamp')
  addedToLocalDbDate: string;

  @Column('date')
  publishedDate: string;

  @Column('date')
  lastModifiedDate: string;

  @Column({
    type: 'jsonb',
    array: false,
    default: () => "'[]'",
    nullable: false,
  })
  public cpe_list: Array<{ cpe23Uri: string; vulnerable: boolean }>;
}
