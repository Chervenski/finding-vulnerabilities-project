import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Cpe } from './cpe.entity';
import { Cve } from './cve.entity';

@Entity('products')
export class Products {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  vendor: string;

  @Column()
  version: string;

  @Column({ default: false })
  vulnerable: boolean;

  @Column({ default: false })
  match: boolean;

  @CreateDateColumn()
  createdOn: Date;

  @ManyToMany(type => Cpe)
  @JoinTable()
  cpe_list: Cpe[];

  @ManyToMany(type => Cve)
  @JoinTable()
  cve_list: Cve[];
}
