import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cpes')
export class Cpe {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  cpe23Uri: string;

  @Column({ nullable: true })
  product: string;

  @Column({ nullable: true })
  vendor: string;

  @Column({ nullable: true })
  version: string;
}
