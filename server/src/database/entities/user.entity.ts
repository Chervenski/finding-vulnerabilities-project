import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
} from 'typeorm';
import { Role } from './role.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  joinedOn: string;

  @Column('bytea', { nullable: true, default: null })
  avatar: Buffer;

  @Column({ type: 'text' })
  username: string;

  @Column()
  password: string;

  @Column({ type: 'text', unique: true })
  email: string;

  @ManyToMany(
    type => Role,
    role => role.users,
  )
  @JoinTable()
  roles: Role[];
}
