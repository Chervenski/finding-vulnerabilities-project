import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { InventoryModule } from './inventory/inventory.module';
import { ProductsModule } from './products/products.module';
import { CoreModule } from './resources/core.module';
import { UsersModule } from './users/users.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    DatabaseModule,
    UsersModule,
    CoreModule,
    ProductsModule,
    InventoryModule,
  ],
})
export class AppModule {}
