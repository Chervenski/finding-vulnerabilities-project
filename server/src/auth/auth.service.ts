import { BadRequestException, Injectable, UnauthorizedException, ConflictException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/database/entities/user.entity';
import { ResponseMessageDTO } from '../resources/models/response.message.dto';
import { UserRegisterDTO } from '../resources/models/user/user.register.dto';
import { Repository } from 'typeorm';
import { UserReturnDTO } from '../resources/models/user/user.return.dto';
import { UserLoginDTO } from '../resources/models/user/user.login.dto';
import { plainToClass } from 'class-transformer';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  private readonly tokensBlacklist: string[] = [];

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  public async register(body: UserRegisterDTO): Promise<ResponseMessageDTO> {
    const { name, email, username, password } = body;
    const salt = await bcrypt.genSalt();

    if (!await this.uniqueUser(email)) {
      const user = new User();
      user.name = name;
      user.email = email;
      user.username = username;
      user.password = await this.hashPassword(password, salt);
      
      await this.userRepository.save(user);
  
      return {
        msg: `Successful registration!`,
      };
    }
  }

  public async loginUser(loginUser: UserLoginDTO): Promise<{ token: string }> {
    const user: UserReturnDTO = await this.validateUser(loginUser);

    if (!user) {
      throw new BadRequestException('Wrong username/email or password!');
    }
    const payload: UserReturnDTO = { ...user };

    return { token: await this.jwtService.signAsync(payload) };
  }

  public tokenAddToBlacklist(token: string): void {
    this.tokensBlacklist.push(token);
  }

  public tokenCheckIfBlacklisted(token: string): boolean {
    return this.tokensBlacklist.includes(token);
  }

  private async validateUser(user: UserLoginDTO): Promise<UserReturnDTO> {
    let userValidated: User;
    
    if (user.email) {
      userValidated = await this.userRepository.findOne({
        email: user.email,
      });
    } else {
      throw new BadRequestException(`Wrong login details.`);
    }

    if (userValidated === undefined) {
      return null;
    }

    if (!(await bcrypt.compare(user.password, userValidated.password))) {
      return null;
    }

    return plainToClass(UserReturnDTO, userValidated, {
      excludeExtraneousValues: true,
    });
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  
  private async uniqueUser(email: string): Promise<boolean> {
    const user = await this.userRepository.findOne({ email: email });

    if (!user) {
      return false;
    }

    throw new ConflictException(`Email: ${user.email} has already been token`);
  }
}
