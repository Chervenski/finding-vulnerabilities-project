import { Controller, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserRegisterDTO } from 'src/resources/models/user/user.register.dto';
import { ResponseMessageDTO } from 'src/resources/models/response.message.dto';
import { UserLoginDTO } from '../resources/models/user/user.login.dto';
import { Token } from '../resources/decorators/token.decorator';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/resources/guards/admin.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/registration')
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  public async register(
    @Body() body: UserRegisterDTO,
  ): Promise<ResponseMessageDTO> {
    return await this.authService.register(body);
  }

  @Post()
  public async login(@Body() user: UserLoginDTO): Promise<{ token: string }> {
    return await this.authService.loginUser(user);
  }

  @Delete()
  public logout(@Token() token: string): ResponseMessageDTO {
    this.authService.tokenAddToBlacklist(token);

    return { msg: 'Logout successful!' };
  }
}
