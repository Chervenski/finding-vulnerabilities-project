import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/resources/models/user/user.dto';
import { UserLoginDTO } from 'src/app/resources/models/user/user-login.dto';
import { ResponseMessageDTO } from 'src/app/resources/models/response.message.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly helper = new JwtHelperService();
  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserLoggedIn()
  );
  private readonly loggedUserSubject$ = new BehaviorSubject<User>(
    this.loggedUser()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router
  ) {}

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<User> {
    return this.loggedUserSubject$.asObservable();
  }

  public register(user): Observable<ResponseMessageDTO> {
    return this.http.post<ResponseMessageDTO>(
      'http://localhost:3000/auth/registration',
      user
    );
  }

  public login(user: UserLoginDTO) {
    return this.http
      .post<{ token: string }>(`http://localhost:3000/auth`, user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser = this.helper.decodeToken(token);
            this.storage.save('token', token);

            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);

            this.router.navigate(['/products']);
          } catch (error) {}
        })
      );
  }

  public logout() {
    this.storage.save('token', '');
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);
  }

  public getUserInfo(id: number): Observable<User> {
    return this.http.get<User>(`http://localhost:3000/user/${id}`);
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): User {
    try {
      return this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
