import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AddProduct } from 'src/app/resources/models/products/add-product.dto';
import { Product } from 'src/app/resources/models/products/product.dto';
import { ResponseMessageDTO } from 'src/app/resources/models/response.message.dto';
import { ProductDetails } from 'src/app/resources/models/products/product.details.dto';
import { SearchDTO } from 'src/app/resources/models/search.dto';

@Injectable()
export class ProductService {
  constructor(private readonly http: HttpClient) {}

  public allProducts(): Observable<Product[]> {
    return this.http.get<Product[]>('http://localhost:3000/products');
  }

  public getProductDetails(productId: number): Observable<ProductDetails> {
    return this.http.get<ProductDetails>(
      `http://localhost:3000/products/${productId}`
    );
  }

  public deleteProduct(productId: number): Observable<ResponseMessageDTO> {
    return this.http.delete<ResponseMessageDTO>(
      `http://localhost:3000/products/${productId}`
    );
  }

  public getCves(link: string): Observable<ResponseMessageDTO> {
    return this.http.post<ResponseMessageDTO>(
      'http://localhost:3000/inventory/',
      link
    );
  }

  public refreshCves(): Observable<ResponseMessageDTO> {
    return this.http.get<ResponseMessageDTO>(
      'http://localhost:3000/inventory/refresh'
    );
  }

  public deleteCveRelation(
    productId: number,
    cveId: number
  ): Observable<ResponseMessageDTO> {
    return this.http.delete<ResponseMessageDTO>(
      `http://localhost:3000/products/${productId}/cve/${cveId}`
    );
  }

  public deleteCpeRelation(
    productId: number,
    cpeId: number
  ): Observable<ResponseMessageDTO> {
    return this.http.delete<ResponseMessageDTO>(
      `http://localhost:3000/products/${productId}/cpe/${cpeId}`
    );
  }

  public addCpeToProductManually(
    productId: number,
    cpeId: number
  ): Observable<ResponseMessageDTO> {
    return this.http.put<ResponseMessageDTO>(
      `http://localhost:3000/products/${productId}/cpe/${cpeId}`,
      {}
    );
  }

  public addCveToProductManually(
    productId: number,
    cveId: number
  ): Observable<ResponseMessageDTO> {
    return this.http.put<ResponseMessageDTO>(
      `http://localhost:3000/products/${productId}/cve/${cveId}`,
      {}
    );
  }

  public addProduct(product: AddProduct): Observable<ResponseMessageDTO> {
    return this.http.post<ResponseMessageDTO>(
      'http://localhost:3000/products',
      product
    );
  }

  public searchProduct(search: any): Observable<any> {
    return this.http.post<any>(
      `http://localhost:3000/products/search?query=${search.search}`,
      {}
    );
  }
}
