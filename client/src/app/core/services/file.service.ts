import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FileService {
  constructor(private readonly httpClient: HttpClient) {}

  public uploadUserImage(id: number, img: File): Observable<void> {
    const fd = new FormData();
    fd.append('file', img);

    return this.httpClient.post<void>(
      `http://localhost:3000/user/${id}/avatar`,
      fd
    );
  }

  public uploadInventory(inventory: File): Observable<any> {
    const fd = new FormData();
    fd.append('file', inventory);

    return this.httpClient.post<void>(
      'http://localhost:3000/products/upload',
      fd
    );
  }

  public convertFileFormat(avatarUrl): any {
    if (avatarUrl) {
      const convertArr = new Uint8Array(avatarUrl.data);
      const convertString = convertArr.reduce((data, byte) => {
        return data + String.fromCharCode(byte);
      }, '');

      const base64String = btoa(convertString);
      return base64String;
    }
  }
}
