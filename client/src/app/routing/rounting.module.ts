import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllProductsComponent } from '../components/all-products/all-products.component';
import { DetailProductComponent } from '../components/detail-product/detail-product.component';
import { HomeComponent } from '../components/home/home.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { SearchPageComponent } from '../components/search-page/search-page.component';
import { UserPageComponent } from '../components/user-page/user-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'profile', component: UserPageComponent },
  { path: 'products', component: AllProductsComponent },
  { path: 'products/:id', component: DetailProductComponent },
  { path: 'search', component: SearchPageComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: 'not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RountingModule {}
