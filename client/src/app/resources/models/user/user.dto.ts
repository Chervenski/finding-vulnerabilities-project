export class User {
    id: number;
    name: string;
    username: string;
    email: string;
    joinedOn: string;
    avatar: string;
}
