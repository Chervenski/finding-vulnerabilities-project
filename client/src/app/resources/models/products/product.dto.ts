export class Product {
  id: number;
  name: string;
  vendor: string;
  version: string;
  createdOn: string;
  vulnerable: boolean;
  match: boolean;
}
