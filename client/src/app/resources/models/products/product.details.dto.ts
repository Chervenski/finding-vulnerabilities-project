export class ProductDetails {
  id: number;
  name: string;
  vendor: string;
  version: string;
  createdOn: string;
  vulnerable: boolean;
  match: boolean;
  cpe_list: any[];
  cve_list: any[];
}
