export class ImageSnippet {
    public pending: boolean = false;
    public status: string = 'init';
    public src: string;
    public file: File;
  
    constructor(src: string, file: File) {
      this.src = src;
      this.file = file;
    }
  }
  