import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CoreModule } from './core/core.module';
import { RountingModule } from './routing/rounting.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderTopComponent } from './components/header/header-top/header-top.component';
import { HeaderBottomComponent } from './components/header/header-bottom/header-bottom.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { AllProductsComponent } from './components/all-products/all-products.component';
import { UserPageComponent } from './components/user-page/user-page.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './resources/interceptors/token.interceptor';
import { DetailProductComponent } from './components/detail-product/detail-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    HeaderTopComponent,
    HeaderBottomComponent,
    AddProductComponent,
    NotFoundComponent,
    SearchPageComponent,
    AllProductsComponent,
    UserPageComponent,
    DetailProductComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    RountingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
