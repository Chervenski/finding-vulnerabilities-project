import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product.service';
import { Product } from 'src/app/resources/models/products/product.dto';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
  public searchGroup: FormGroup;
  public searchByNameOrVendor: boolean;
  public searchByCpe: boolean;
  public searchByCve: boolean;
  public products;
  public cve;
  public cpe;

  constructor(
    private readonly productService: ProductService,
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  public initForm() {
    this.searchGroup = new FormGroup({
      search: new FormControl('', [Validators.required]),
    });
  }

  public searchProduct() {
    this.productService.searchProduct(this.searchGroup.value).subscribe((res) => {
      if (this.searchGroup.value.search.includes('cve')) {
        this.cve = res;
        this.searchByNameOrVendor = false;
        this.searchByCve = true;
        this.searchByCpe = false;
      } else if (this.searchGroup.value.search.includes('cpe')) {
        this.cpe = res;
        this.searchByNameOrVendor = false;
        this.searchByCpe = true;
        this.searchByCve = false;
      } 
      else {
        this.products = res;
        this.searchByNameOrVendor = true;
      }
    });
  }
}
