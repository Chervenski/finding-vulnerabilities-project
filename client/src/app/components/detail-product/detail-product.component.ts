import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../../resources/models/products/product.dto';
import { ProductService } from '../../core/services/product.service';
import { ProductDetails } from '../../resources/models/products/product.details.dto';
import { NotificatorService } from '../../core/services/notificator.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css'],
})
export class DetailProductComponent implements OnInit {
  private productId: number;
  product: ProductDetails;

  constructor(
    private readonly router: Router,
    private readonly productService: ProductService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((param) => {
      this.productId = +param['id'];
      this.getProduct(this.productId);
    });
  }

  public getProduct(productId: number): void {
    this.productService.getProductDetails(productId).subscribe((res) => {
      this.product = res;
    });
  }
  public deleteCve(cveId: number): void {
    console.log('asd');
    this.productService
      .deleteCveRelation(this.productId, cveId)
      .subscribe((res) => {
        this.notificator.success(res.msg);
        this.ngOnInit();
      });
  }
  public deleteCpe(cpeId: number): void {
    this.productService
      .deleteCpeRelation(this.productId, cpeId)
      .subscribe((res) => {
        this.notificator.success(res.msg);
        this.ngOnInit();
      });
  }
}
