import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/resources/models/user/user.dto';
import { FileService } from 'src/app/core/services/file.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { ImageSnippet } from 'src/app/resources/models/image/image.snippet';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from '../../core/services/product.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css'],
})
export class UserPageComponent implements OnInit {
  public inviteGroup: FormGroup;
  public urlId: number = 1;
  public userId: number;
  public selectedFile: ImageSnippet;
  public loggedUser: User;
  public imgUrl: SafeUrl;

  public selectedCve: string;

  constructor(
    private readonly authService: AuthService,
    private readonly fileService: FileService,
    private readonly notificator: NotificatorService,
    private readonly domSanizater: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.authService.loggedUser$.subscribe((user) => {
      this.loggedUser = user;
      this.userId = user.id;

      this.authService.getUserInfo(this.userId).subscribe((user) => {
        if (!user.avatar) {
          this.imgUrl = null;
        } else {
          const base64String = this.fileService.convertFileFormat(user.avatar);
          this.imgUrl = this.domSanizater.bypassSecurityTrustUrl(
            'data:image/jpg;base64, ' + base64String
          );
        }
      });
    });

    this.initForm();
  }

  public initForm() {
    this.inviteGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public inviteUser(): void {
    this.authService.register(this.inviteGroup.value).subscribe((res) => {
      this.notificator.success(res.msg);
    });

    this.inviteGroup.reset();
  }

  public uploadImage(imageInput: any): void {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.fileService
        .uploadUserImage(this.userId, this.selectedFile.file)
        .subscribe(() => {
          this.notificator.success('Uploaded successfully!');
        });
    });

    reader.readAsDataURL(file);

    this.authService.getUserInfo(this.userId).subscribe((res) => {
      const base64String = this.fileService.convertFileFormat(res.avatar);
      this.imgUrl = this.domSanizater.bypassSecurityTrustUrl(
        'data:image/jpg;base64, ' + base64String
      );
    });

    window.location.reload();
  }
}
