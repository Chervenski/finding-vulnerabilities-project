import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  public productGroup: FormGroup;

  constructor(
    private readonly productService: ProductService,
    private readonly notificator: NotificatorService,
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.productGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      vendor: new FormControl('', [Validators.required]),
      version: new FormControl('', [Validators.required]),
    });
  }

  public addProduct() {
    this.productService.addProduct(this.productGroup.value).subscribe(
      (res) => {
        this.notificator.success('Successfully added!');
      (err) => {
        this.notificator.error(err);
      }
    });

    this.productGroup.reset();
  }
}
