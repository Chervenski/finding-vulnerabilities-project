import { Component, OnInit } from '@angular/core';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public loginGroup: FormGroup;

  public faLock = faLock;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
  ) {}

  public ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.loginGroup = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public logIn() {
    this.authService.login(this.loginGroup.value)
      .subscribe(
        (res) => {
          console.log(res);
          this.notificator.success('Login successful!');
        },
        (err) => {
          console.log(err);
          this.notificator.error('Wrong credentials!');
        }
      );
  }
}
