import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/resources/models/user/user.dto';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-header-top',
  templateUrl: './header-top.component.html',
  styleUrls: ['./header-top.component.css']
})
export class HeaderTopComponent implements OnInit {
  @Input() user: User;

  constructor(
    private readonly route: Router,
    private readonly authService: AuthService,
  ) {}

  ngOnInit(): void {}

  public logout() {
    this.authService.logout();
    this.route.navigate(['']);
  }
}
