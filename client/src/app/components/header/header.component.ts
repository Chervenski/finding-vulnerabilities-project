import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/resources/models/user/user.dto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() user: User;

  constructor() {}

  ngOnInit(): void {
  }

}
