import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/core/services/product.service';
import { Product } from 'src/app/resources/models/products/product.dto';
import { FileService } from 'src/app/core/services/file.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
})
export class AllProductsComponent implements OnInit {
  public cveGroup: FormGroup;
  public fileToUpload: File = null;
  public allProducts: Product[];
  public products: Product[];
  public vulnerable: Product[];

  constructor(
    private readonly productService: ProductService,
    private readonly fileService: FileService,
    private readonly notificator: NotificatorService
  ) {}

  ngOnInit(): void {
    this.getProducts();
    this.initForm();
  }

  public initForm() {
    this.cveGroup = new FormGroup({
      link: new FormControl('', [Validators.required]),
    });
  }

  public getProducts(): void {
    this.productService.allProducts().subscribe((res) => {
      this.products = res;
      this.allProducts = res;
      this.vulnerable = res.filter((product) => product.vulnerable);
    });
  }

  public deleteProduct(productId: number): void {
    this.productService.deleteProduct(productId).subscribe((res) => {
      this.notificator.success(res.msg);
      this.ngOnInit();
    });
  }

  public changeTab(tabSelected: number): void {
    if (tabSelected === 1) {
      this.products = this.allProducts;
    }
    if (tabSelected === 2) {
      this.products = this.vulnerable;
    }
  }

  public requestCves() {
    this.productService.getCves(this.cveGroup.value).subscribe((res) => {
      this.notificator.success(res.msg);
    });
  }
  public refreshCves() {
    this.productService.refreshCves().subscribe((res) => {
      this.notificator.success(res.msg);
    });
  }

  public uploadInventory(fileToUpload: FileList) {
    this.fileToUpload = fileToUpload.item(0);

    this.fileService.uploadInventory(this.fileToUpload).subscribe((res) => {
      this.notificator.success(
        `${res.length} new products uploaded successfully!`
      );
      this.ngOnInit();
    });
  }
}
